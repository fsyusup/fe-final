import Link from "next/link";

export default function Topnav() {
  return (
    <div className="card-shadow">
      <div className="user-menu-profile">
        <img
          className="img-profile"
          src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
        />
        <div className="info-profile">
          <div className="name">Yusup Firdaus</div>
          <div className="location">Jagakarsa, Jakarta Selatan</div>
          <div className="saldo">
            <span>Rp. 0</span>
            <button className="topup">+ Topup</button>
          </div>
          {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
        </div>
      </div>
      <div className="user-list-menu">
        <Link href="/user" tag="a">
          Dashboard
        </Link>
        <Link href="/admin" tag="a">
          Permintaan Pekerjaan
        </Link>
        <Link href="/user/transaction" tag="a">
          Transaksi
        </Link>
        <Link href="/user/transaction" tag="a">
          Akun
        </Link>
        <Link href="/contact-us" tag="a">
          Keluar
        </Link>
      </div>
    </div>
  );
}
