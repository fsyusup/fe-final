import Link from "next/link";

export default function Topnav() {
  return (
    <div className="topnav topnav-transparent" id="myTopnav">
    <div className="topnav-wrapper container-large">
      <div className="topnav-logo">
        Sepaket
      </div>
      {/* <div className="topnav-search">
        <input type="text"/>
      </div> */}
      <div className="topnav-left">
        <Link href="/" tag="a">
          Home
        </Link>
        <Link href="/admin" tag="a">
          Admin
        </Link>
        <Link href="/service" tag="a">
          Service
        </Link>
        <Link href="/contact-us" tag="a">
          Contact Us
        </Link>
      </div>
      <div className="topnav-right">
        <Link href="/login" tag="a">
          Login
        </Link>
        <Link href="/register" tag="a">
          Register
        </Link>
      </div>
    </div>
  </div>
  )
}