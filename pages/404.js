import Link from "next/link";
import { useRouter } from "next/router";

const IndexPage = (props) => (
  <div className="container-small mt-30">
    <div className="card-shadow pd-30">
     Sorry page not found
    </div>
  </div>
);

IndexPage.layout = "default";

export default IndexPage;
