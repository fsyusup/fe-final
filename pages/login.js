import Link from "next/link";
import { useRouter } from "next/router";

const IndexPage = (props) => (
  <div className="container-small mt-30">
    <div className="card-shadow pd-30">
      <span className="card-title">Login</span>
      <div className="form-horizontal mt-30">
        <div className="form-group">
          <label>Email</label>
          <input type="text" className="input-text" />
        </div>
        <div className="form-group">
          <label>Password</label>
          <input type="text" className="input-text" />
        </div>
        <div className="form-action">
        <Link href="/user" tag="a">
          <button className="btn btn-primary full">Login</button>
          </Link>
        </div>
        <div className="mt-30">
          Baru di sepaket?
          <Link  href="/register" tag="a" >
            Daftar
          </Link>
        </div>
      </div>
    </div>
  </div>
);

IndexPage.layout = "default";

export default IndexPage;
