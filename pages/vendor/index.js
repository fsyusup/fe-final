import { useRouter } from "next/router";
import React, { useState } from "react";
import Carousel, { Dots, arrowsPlugin } from "@brainhubeu/react-carousel";
import "@brainhubeu/react-carousel/lib/style.css";

import Link from "next/link";

const Post = () => {
  const router = useRouter();
  const { slug } = router.query;
  const [tab, setTab] = useState("description");
  const [slider, setSlider] = useState(0);

  const onChangeSlider = (value) => {
    setSlider(value);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Post.layout = "default";

export default Post;
