import Link from "next/link";

const IndexPage = (props) => (
  <div className="container-small mt-30">
    <div className="card-shadow pd-30">
      <span className="card-title">Daftar</span>
      <div className="form-horizontal mt-30">
        <div className="form-group">
          <label>Nama Lengkap</label>
          <input type="text" className="input-text"  />
        </div>
        <div className="form-group">
          <label>No. Handphone</label>
          <input type="text" className="input-text"  />
        </div>
        <div className="form-group">
          <label>Email</label>
          <input type="text" className="input-text"  />
        </div>
        <div className="form-group">
          <label>Password</label>
          <input type="text" className="input-text" />
        </div>
        <div className="form-group">
          <label>Konfirmasi Password</label>
          <input type="text" className="input-text" />
        </div>
        <div className="form-action">
          <button className="btn btn-primary full">Daftar</button>
        </div>
        <div className="mt-30">
          Sudah punya akun?
          <Link href="/login" tag="a">
            Login
          </Link>
        </div>
      </div>
    </div>
  </div>
);

IndexPage.layout = "default";

export default IndexPage;
