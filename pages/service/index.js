import Link from "next/link";
import { useRouter } from "next/router";

const IndexPage = (props) => (
  <div className="container mt-10">
    <div className="card-shadow pd-5 mb-15">
      <div className="service">
      <img className="img-thumb" src="https://cf.shopee.co.id/file/b734ab62646d5a2d8250bacab9cf732f" />
      <div className="service-content">
      <Link  href="/service/tes" tag="a" >
      <div className="card-title">Jasa pembuatan baju custom (bisa satuan)</div>
      </Link>
      <div className="service-location"><img className="icon-location" src="https://www.flaticon.com/svg/vstatic/svg/860/860762.svg?token=exp=1611931357~hmac=a10a87787590727c865a9ecf3fc57216"/>Jagakarsa, Jakarta Selatan</div>
      <p>Buy authentic malnad banana chips without any salt or masala added. Bananans are</p>
      <span className="service-price">Rp. 12.000.000</span>
      </div>
      </div>
    </div>
    <div className="card-shadow pd-5 mb-15">
      <div className="service">
      <img className="img-thumb" src="https://cf.shopee.co.id/file/b734ab62646d5a2d8250bacab9cf732f" />
      <div className="service-content">
      <div className="card-title">Jasa pembuatan baju custom (bisa satuan)</div>
      <div className="service-location"><img className="icon-location" src="https://www.flaticon.com/svg/vstatic/svg/860/860762.svg?token=exp=1611931357~hmac=a10a87787590727c865a9ecf3fc57216"/>Jagakarsa, Jakarta Selatan</div>
      <p>Buy authentic malnad banana chips without any salt or masala added. Bananans are</p>
      <span className="service-price">Rp. 12.000.000</span>
      </div>
      </div>
    </div>
    <div className="card-shadow pd-5 mb-15">
      <div className="service">
      <img className="img-thumb" src="https://cf.shopee.co.id/file/b734ab62646d5a2d8250bacab9cf732f" />
      <div className="service-content">
      <div className="card-title">Jasa pembuatan baju custom (bisa satuan)</div>
      <div className="service-location"><img className="icon-location" src="https://www.flaticon.com/svg/vstatic/svg/860/860762.svg?token=exp=1611931357~hmac=a10a87787590727c865a9ecf3fc57216"/>Jagakarsa, Jakarta Selatan</div>
      <p>Buy authentic malnad banana chips without any salt or masala added. Bananans are</p>
      <span className="service-price">Rp. 12.000.000</span>
      </div>
      </div>
    </div>
    <div className="card-shadow pd-5 mb-15">
      <div className="service">
      <img className="img-thumb" src="https://crocodaily.com/assets/images/banana-chips-plain.jpg" />
      <div className="service-content">
      <span className="card-title">Banana Chips Plain</span>
      <p>Buy authentic malnad banana chips without any salt or masala added. Bananans are</p>
      <span className="service-price">Rp. 12.000.000</span>
      </div>
      </div>
    </div>
    <div className="card-shadow pd-5 mb-15">
      <div className="service">
      <img className="img-thumb" src="https://crocodaily.com/assets/images/banana-chips-plain.jpg" />
      <div className="service-content">
      <span className="card-title">Banana Chips Plain</span>
      <p>Buy authentic malnad banana chips without any salt or masala added. Bananans are</p>
      <span className="service-price">Rp. 12.000.000</span>
      </div>
      </div>
    </div>
    <div className="card-shadow pd-5 mb-15">
      <div className="service">
      <img className="img-thumb" src="https://crocodaily.com/assets/images/banana-chips-plain.jpg" />
      <div className="service-content">
      <span className="card-title">Banana Chips Plain</span>
      <p>Buy authentic malnad banana chips without any salt or masala added. Bananans are</p>
      <span className="service-price">Rp. 12.000.000</span>
      </div>
      </div>
    </div>
    <div className="card-shadow pd-5 mb-15">
      <div className="service">
      <img className="img-thumb" src="https://crocodaily.com/assets/images/banana-chips-plain.jpg" />
      <div className="service-content">
      <span className="card-title">Banana Chips Plain</span>
      <p>Buy authentic malnad banana chips without any salt or masala added. Bananans are</p>
      <span className="service-price">Rp. 12.000.000</span>
      </div>
      </div>
    </div>
  </div>
);

IndexPage.layout = "default";

export default IndexPage;
