import { useRouter } from "next/router";
import React, { useState } from "react";
import Carousel, { Dots, arrowsPlugin } from "@brainhubeu/react-carousel";
import "@brainhubeu/react-carousel/lib/style.css";

import Link from "next/link";

const Post = () => {
  const router = useRouter();
  const { slug } = router.query;
  const [tab, setTab] = useState("description");
  const [slider, setSlider] = useState(0);

  const onChangeSlider = (value) => {
    setSlider(value);
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-8">
          <div className="card-shadow mt-15">
            <div className="pd-15">
              <div className="slider-wrapper">
                <Carousel
                  value={slider}
                  onChange={onChangeSlider}
                  plugins={[
                    {
                      resolve: arrowsPlugin,
                      options: {
                        arrowLeft: <button> L </button>,
                        arrowLeftDisabled: <button> LL </button>,
                        arrowRight: <button> RR</button>,
                        arrowRightDisabled: <button>R</button>,
                        addArrowClickHandler: true,
                      },
                    },
                  ]}
                >
                  <img
                    className="slider-img"
                    src="https://picsum.photos/seed/picsum/500/300?t=1"
                  />
                  <img
                    className="slider-img"
                    src="https://crocodaily.com/assets/images/banana-chips-spicy-masala.jpg"
                  />
                  <img
                    className="slider-img"
                    src="https://picsum.photos/seed/picsum/500/300?t=3"
                  />
                </Carousel>
              </div>
              <div className="slider-dots">
                <Dots
                  value={slider}
                  onChange={onChangeSlider}
                  thumbnails={[
                    <img
                      key={0}
                      className="slider-dots-img"
                      src="https://picsum.photos/seed/picsum/500/300?t=1"
                    />,
                    <img
                      key={1}
                      className="slider-dots-img"
                      src="https://crocodaily.com/assets/images/banana-chips-spicy-masala.jpg"
                    />,
                    <img
                      key={3}
                      className="slider-dots-img"
                      src="https://picsum.photos/seed/picsum/500/300?t=3"
                    />,
                  ]}
                />
              </div>
            </div>
            <div className="service">
              <div className="service-content">
                <Link href="/service/tes" tag="a">
                  <div className="card-title">
                    Jasa pembuatan baju custom (bisa satuan)
                  </div>
                </Link>
                <div className="service-location">
                  <img
                    className="icon-location"
                    src="https://www.flaticon.com/svg/vstatic/svg/860/860762.svg?token=exp=1611931357~hmac=a10a87787590727c865a9ecf3fc57216"
                  />
                  Jagakarsa, Jakarta Selatan
                </div>

                <span className="service-price mt-10">Rp. 12.000.000</span>
              </div>
            </div>
            <div className="tab">
              <label
                className={`${tab == "description" ? "active" : ""}`}
                onClick={() => setTab("description")}
              >
                Deskripsi
              </label>
              <label
                className={`${tab == "note" ? "active" : ""}`}
                onClick={() => setTab("note")}
              >
                Catatan
              </label>
              <label
                className={`${tab == "review" ? "active" : ""}`}
                onClick={() => setTab("review")}
              >
                Ulasan
              </label>
            </div>
            {tab == "description" && (
              <div className="tab-body">
                <p>
                  Defining routes by using predefined paths is not always enough
                  for complex applications. In Next.js you can add brackets to a
                  page ([param]) to create a dynamic route (a.k.a. url slugs,
                  pretty urls, and others).
                </p>
                <p>
                  Any route like /post/1, /post/abc, etc. will be matched by
                  pages/post/[pid].js. The matched path parameter will be sent
                  as a query parameter to the page, and it will be merged with
                  the other query parameters.
                </p>
              </div>
            )}
            {tab == "note" && (
              <div className="tab-body">
                <p>
                  Pengadaan barang/jasa Pemerintah yang efisien dan efektif
                  merupakan salah satu bagian yang penting dalam perbaikan
                  pengelolaan keuangan negara. Salah satu perwujudannya adalah
                  dengan pelaksanaan proses pengadaan barang/jasa Pemerintah
                  secara elektronik, yaitu pengadaan barang/jasa yang
                  dilaksanakan dengan menggunakan teknologi informasi dan
                  transaksi elektronik sesuai dengan ketentuan peraturan
                  perundang-undangan.
                </p>
                <p>
                  Penyelenggaraan pengadaan barang/jasa Pemerintah secara
                  elektronik diatur dalam Peraturan Presiden Nomor 16 Tahun 2018
                  pengadaan barang/jasa Pemerintah. Selain itu sebagaimana
                  ketentuan pasal 50 ayat (5) pada Peraturan Presiden Nomor 16
                  Tahun 2018 K/L/PD wajib melaksanakan pengadaan barang/jasa
                  melalui metode e-Purchasing yang menyangkut pemenuhan
                  kebutuhan nasional dan/atau strategis yang ditetapkan oleh
                  menteri, kepala lembaga, atau kepala daerah. Selain itu pada
                  Peraturan Lembaga Kebijakan Pengadaan Barang/Jasa Nomor 9
                  Tahun 2018 menjadikan pedoman selaku K/L/PD dalam melakukan
                  proses pengadaan barang/jasa melalui penyedia.
                </p>
                <p>
                  Proses pengadaan barang/jasa Pemerintah secara elektronik ini
                  akan lebih meningkatkan dan menjamin terjadinya efisiensi,
                  efektifitas, transparansi, dan akuntahapanilitas dalam
                  pembelanjaan uang negara. Selain itu, proses pengadaan
                  barang/jasa Pemerintah secara elektronik ini juga dapat lebih
                  menjamin tersedianya informasi, kesempatan usaha, serta
                  mendorong terjadinya persaingan yang sehat dan terwujudnya
                  keadilan (non discriminative) bagi seluruh pelaku usaha yang
                  bergerak dibidang pengadaan barang/jasa Pemerintah.{" "}
                </p>

                <p>
                  e-Purchasing dibuat agar proses untuk pengadaan produk
                  barang/jasa Pemerintah dapat dilakukan secara elektronik.
                  Dalam ePurchasing produk barang/jasa Pemerintah, terdapat
                  fitur untuk pembuatan paket, unduh (download) format surat
                  pesanan/surat perjanjian, unggah (upload) hasil scan kontrak
                  yang sudah ditandatangani, sampai dengan cetak pesanan produk
                  barang/jasa Pemerintah. Dengan adanya e-Purchasing produk
                  barang/jasa Pemerintah, diharapkan proses pengadaan produk
                  barang/jasa Pemerintah dapat lebih efisien dan lebih
                  transparan.
                </p>
                <p>
                  Produk yang sudah tampil di Katalog Elektronik produk
                  barang/jasa Pemerintah dapat dibeli dengan menggunakan
                  ePurchasing. Katalog Elektronik produk barang/jasa Pemerintah
                  menampilkan informasi penyedia produk, spesifikasi produk,
                  harga, serta gambar dari produk barang/jasa Pemerintah.
                </p>
              </div>
            )}
            {tab == "review" && (
              <div className="tab-body">
                <div className="review-list">
                  <div className="review-item">
                    <img src="https://www.flaticon.com/svg/vstatic/svg/860/860762.svg?token=exp=1611931357~hmac=a10a87787590727c865a9ecf3fc57216" />
                    <div className="review-body">
                      <div className="review-user"></div>
                      <p>Wow sekali</p>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className="col-4">
          <div className="card-shadow mt-15">
            <div className="vendor-info">
              <img
                className="img-profile"
                src="https://www.flaticon.com/svg/vstatic/svg/237/237382.svg?token=exp=1611972585~hmac=faa6f516d98ca9e3a483612272d77caa"
              />
              <div className="info-profile">
                <div className="name">PT. Sentosa Jaya Abadi</div>
                <div className="location">Jagakarsa, Jakarta Selatan</div>
                <div className="service">
                  <span>23 Layanan</span>
                  <Link href="/user" tag="a">
                    Lihat
                  </Link>
                </div>
                <div className="rating">
                  <span>4.3 Luar biasa</span>
                </div>
                {/* <img src="https://www.flaticon.com/svg/vstatic/svg/1159/1159633.svg?token=exp=1611974740~hmac=42410159b4f668bd89a8a38e6a6ef62b"/> */}
              </div>
            </div>
            <div className="pd-15">
              <div className="transaction-warning mb-15">
                Transaksi diluar platform sepaket bukan menjadi tanggung jawab
                kami.
              </div>
              <div className="service-detail-transaction">
                <span className="package-label">Paket</span>
                <div className="package-selected mb-15"> Regular</div>
                <span className="price-label">Mulai dari</span>
                <div className="price mb-15">Rp. 1.200.000</div>
              </div>
              <Link href="/user" tag="a">
                <button className="btn btn-primary full">
                  Buat Permintaan
                </button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Post.layout = "default";

export default Post;
