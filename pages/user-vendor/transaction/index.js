import Link from "next/link";
import { useRouter } from "next/router";

const IndexPage = (props) => (
  <div className="user-content card-shadow">
    <div className="user-content-header">
      <h1>Daftar Transaksi</h1>
    </div>
      <div className="user-content-menu">
        <label>Semua</label>
        <label>History</label>
      </div>
      <div className="user-content-body pd-15">
        <label>Semua</label>
        <label>History</label>
      </div>
  </div>
);

IndexPage.layout = "vendor";

export default IndexPage;
