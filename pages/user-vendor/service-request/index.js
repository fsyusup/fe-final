import Link from "next/link";
import { useRouter } from "next/router";

const IndexPage = (props) => (
  <div className="container-medium mt-30">
    <div className="card-shadow pd-30">
      <span className="card-title">Login</span>
      <div className="form-horizontal mt-30">
        <div className="form-group">
          <label>Username / Email</label>
          <input type="text" className="input-text" />
        </div>
        <div className="form-group">
          <label>Password</label>
          <input type="text" className="input-text" />
        </div>
        <div className="form-action">
          <button className="btn btn-primary full">Login</button>
        </div>
        <div className="mt-30">
          Baru di sepaket?
          <Link href="/register" tag="a">
            Daftar
          </Link>
        </div>
      </div>
    </div>
  </div>
);

IndexPage.layout = "vendor";

export default IndexPage;
