import Head from "next/head";
import Link from "next/link";
import Topnav from "../components/Topnav"
import UserMenu from "../components/UserMenu"

const DefaultLayout = (props) => (
  <>
    <Head>
      <title>User</title>
      <meta charSet="utf-8" />
    </Head>
     <Topnav/>
     <div className="container-large user-section mt-20">
       <div className="user-menu">
     <UserMenu />
       </div>
      {props.children}
      </div>
  </>
);

export default DefaultLayout;
