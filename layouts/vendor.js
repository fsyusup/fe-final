import Head from "next/head";
import Link from "next/link";
import Topnav from "../components/Topnav"
import VendorMenu from "../components/VendorMenu"

const DefaultLayout = (props) => (
  <>
    <Head>
      <title>Vendor</title>
      <meta charSet="utf-8" />
    </Head>
     <Topnav/>
     <div className="container-large user-section mt-20">
       <div className="user-menu">
     <VendorMenu />
       </div>
      {props.children}
      </div>
  </>
);

export default DefaultLayout;
