import Head from "next/head";
import Link from "next/link";
import Topnav from "../components/Topnav"

const DefaultLayout = (props) => (
  <>
    <Head>
      <title>Default</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      

    </Head>
     <Topnav/>
    
      <div>{props.children}</div>
  </>
);

export default DefaultLayout;
