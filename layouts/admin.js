import Head from "next/head";
import Link from "next/link";

const AdminLayout = (props) => (
  <>
    <Head>
      <title>Admin</title>
      <meta charSet="utf-8" />
    </Head>
    <div>
      <div className="sidenav">
        <Link href="/" tag="a">
          Home
        </Link>
        <Link href="/contact-us" tag="a">
          Service
        </Link>
        <Link href="/contact-us" tag="a">
          Vendor
        </Link>
        <Link href="/contact-us" tag="a">
          User
        </Link>
        <Link href="/contact-us" tag="a">
          Transaction
        </Link>
        
        <Link href="/contact-us" tag="a">
          Contact Us
        </Link>
        <Link href="/contact-us" tag="a">
          Logout
        </Link>
      </div>
      <div id="main">{props.children}</div>
    </div>
  </>
);

export default AdminLayout;
